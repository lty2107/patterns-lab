package Decorator.products;

public interface ProductsSeller {
    String sell();
}
