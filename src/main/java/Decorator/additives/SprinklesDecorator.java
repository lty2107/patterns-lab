package Decorator.additives;

import Decorator.products.ProductsSeller;

public class SprinklesDecorator extends ProductsDecorator{

    public SprinklesDecorator(ProductsSeller decoratedProduct){
        super(decoratedProduct);
    }

    @Override
    public String sell(){
        return decoratedProductsSeller.sell() + " + присыпка";
    }
}
