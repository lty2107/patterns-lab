package Decorator.additives;

import Decorator.products.ProductsSeller;

public class SyropDecorator extends ProductsDecorator {

    public SyropDecorator(ProductsSeller decoratedProduct){
        super(decoratedProduct);
    }

    @Override
    public String sell(){
        return decoratedProductsSeller.sell() + " + сироп";
    }
}
