package Decorator.additives;

import Decorator.products.ProductsSeller;

public class ProductsDecorator implements ProductsSeller {

    protected ProductsSeller decoratedProductsSeller;

    public ProductsDecorator (ProductsSeller decoratedProductsSeller){
        this.decoratedProductsSeller = decoratedProductsSeller;
    }

    @Override
    public String sell() {
        return decoratedProductsSeller.sell();
    }
}
