package Decorator;

import Decorator.additives.SprinklesDecorator;
import Decorator.additives.SyropDecorator;
import Decorator.products.IceScream;
import Decorator.products.Smoothies;
import Decorator.products.Yogurt;

public class Main {

    public static void main(String[] args) {
        IceScream iceScream = new IceScream();
        System.out.println(iceScream.sell());

        SprinklesDecorator sprinklesDecorator = new SprinklesDecorator(new Yogurt());
        System.out.println(sprinklesDecorator.sell());

        SyropDecorator syropDecorator = new SyropDecorator(new SprinklesDecorator(new Smoothies()));
        System.out.println(syropDecorator.sell());
    }
}
