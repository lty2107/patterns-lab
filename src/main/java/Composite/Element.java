package Composite;

public abstract class Element {
    private final String element;

    protected Element(String element) {
        this.element = element;
    }

    public String getElementName(){
        return element;
    }

}
