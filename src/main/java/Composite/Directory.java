package Composite;

import java.util.ArrayList;
import java.util.List;

public class Directory extends Element{

    List<Element> subElements = new ArrayList<>();

    protected Directory(String element) {
        super(element);
    }

    public void showSubElements(){
        for (Element subElement : subElements) {
            System.out.println(getElementName() + subElement.getElementName());
        }

    }

    public void add(Element element){
        subElements.add(element);
    }

    public void remove(Element element){
        subElements.remove(element);
    }
}
