package Composite;

public class Main {

    public static void main(String[] args) {
        Directory directory = new Directory("C://");
        Directory filmsDirectory = new Directory("Films://");
        Directory musicDirectory = new Directory("Music://");

        directory.add(musicDirectory);
        directory.add(filmsDirectory);
        directory.showSubElements();

        File mp3File = new File("music1.mp3");
        musicDirectory.add(mp3File);
        File videoFile = new File("movie18+.mp4");
        filmsDirectory.add(videoFile);

        musicDirectory.showSubElements();
        filmsDirectory.showSubElements();

    }
}
