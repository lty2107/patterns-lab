package Adapter;

import Adapter.adapter.SquarePegAdapter;
import Adapter.hole.RoundHole;
import Adapter.peg.RoundPeg;
import Adapter.peg.SquarePeg;

public class Main {

    public static void main(String[] args) {
        RoundPeg roundPeg = new RoundPeg(3.5);
        RoundHole roundHole = new RoundHole(5.0);
        if (roundHole.fits(roundPeg)){
            System.out.println("Круглый колышек совместим с отверстием");
        }
        else {
            System.out.println("Круглый колышек не совместим с отверстием");
        }

        SquarePeg squarePeg = new SquarePeg(4.8);
        SquarePegAdapter squarePegAdapter = new SquarePegAdapter(squarePeg);
        if (roundHole.fits(squarePegAdapter)){
            System.out.println("Квадратный колышек совместим с отверстием");
        }
        else{
            System.out.println("Квадратный колышек не совместим с отверстием");
        }
    }

}
