package Adapter.hole;

import Adapter.peg.RoundPeg;

public class RoundHole{
    protected Double radius;

    public RoundHole(Double radius){
        this.radius = radius;
    }

    public Double getRadius() {
        return radius;
    }

    public boolean fits(RoundPeg roundPeg) {
        return this.radius >= roundPeg.getRadius();
    }
}
