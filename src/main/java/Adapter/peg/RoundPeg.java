package Adapter.peg;

public class RoundPeg {
    private Double radius;

    public RoundPeg(){

    }

    public RoundPeg(Double radius){
        this.radius = radius;
    }

    public Double getRadius() {
        return radius;
    }
}
