package Adapter.peg;

public class SquarePeg {
    private Double width;

    public SquarePeg(){

    }

    public SquarePeg(Double width){
        this.width = width;
    }

    public Double getWidth() {
        return width;
    }
}
