package Adapter.adapter;

import Adapter.peg.RoundPeg;
import Adapter.peg.SquarePeg;

public class SquarePegAdapter extends RoundPeg {
    private SquarePeg squarePeg;

    public SquarePegAdapter(SquarePeg squarePeg) {
        this.squarePeg = squarePeg;
    }

    @Override
    public Double getRadius(){
        return Math.sqrt(Math.pow(squarePeg.getWidth(), 2) + Math.pow(squarePeg.getWidth(), 2));
    }
}
