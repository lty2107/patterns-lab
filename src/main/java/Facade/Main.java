package Facade;

public class Main {

    public static void main(String[] args) {
        PancakesFacade pancakesFacade = new PancakesFacade();
        pancakesFacade.cookPancakes(8);
    }
}
