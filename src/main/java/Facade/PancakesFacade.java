package Facade;

public class PancakesFacade {

    public void cookPancakes(int count){
        System.out.println("Начинаем готовить обед!\n");
        prepareTableware(count);
        prepareDough();
        for(int i = 1; i <= count; i++){
            System.out.printf("Готовим блин номер %d\n", i);
        }
        System.out.println("Обед готов!");
    }

    private void prepareDough(){
        System.out.println("Готовим тесто");
    }

    private void prepareTableware(int count){
        System.out.printf("Готовим %d тарелок\n", count);
    }
}
